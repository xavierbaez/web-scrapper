const axios = require('axios')
const config = require('config')
const cheerio = require('cheerio')
const express = require('express')

const PORT = config.get('server.port')
const url = config.get('website.protocol') + '://' + config.get('website.URL')
console.log(url)
const app = express()

// Make a request for a user with a given ID
axios(url).then( response => {
    const html = response.data
    console.log(html)
})

app.listen(PORT, () => console.log(`server running on ${PORT}`))